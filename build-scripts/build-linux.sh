#!/bin/sh

PKG_CONFIG_PATH="$(which pkg-config)"
INSTALL_DIR="${INSTALL_DIR:-bin}"

echo "### Building BennuGD Core ###"

cd core
case $1 in
  release)
    ./configure \
      --build=i686-pc-linux-gnu \
      PKG_CONFIG_PATH="${PKG_CONFIG_PATH}" \
      PKG_CONFIG_LIBDIR="${PKG_CONFIG_PATH}" \
      ZLIB_CFLAGS=" " ZLIB_LIBS="-lz" \
      LDFLAGS='-Wl,-rpath,'\''$$ORIGIN'\'',--enable-new-dtags' \
      && make clean && make
    ;;

  *)
    make
    ;;
esac
if [ $? -ne 0 ]; then
  echo "*** ABORT ***"
  exit 1
fi
cd -

echo "### Building BennuGD Modules ###"

cd modules
case $1 in
  release)
    ./configure \
      --build=i686-pc-linux-gnu \
      PKG_CONFIG_PATH=${PKG_CONFIG_PATH} \
      PKG_CONFIG_LIBDIR=${PKG_CONFIG_PATH} \
      PNG_CFLAGS="-I/usr/include/libpng16" PNG_LIBS="-lpng16"\
      ZLIB_CFLAGS=" " ZLIB_LIBS="-lz" \
      SDL_mixer_CFLAGS=" " SDL_mixer_LIBS="-lSDL_mixer" \
      SDL_CFLAGS="-I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT" SDL_LIBS="-lSDL" \
      LDFLAGS='-Wl,-rpath,'\''$$ORIGIN'\'',--enable-new-dtags' \
      && make clean && make
    ;;

  *)
    make
    ;;
esac
if [ $? -ne 0 ]; then
  echo "*** ABORT ***"
  exit 1
fi
cd -

echo "### Building BennuGD Tools ###"

cd tools/moddesc
case $1 in
  release)
    ./configure \
      --build=i686-pc-linux-gnu \
      PKG_CONFIG_PATH=${PKG_CONFIG_PATH} \
      PKG_CONFIG_LIBDIR=${PKG_CONFIG_PATH} \
      LDFLAGS='-Wl,-rpath,'\''$$ORIGIN'\'',--enable-new-dtags' \
      && make clean && make
    ;;

  *)
    make
    ;;
esac
if [ $? -ne 0 ]; then
  echo "*** ABORT ***"
  exit 1
fi
cd -

echo "### Installing in $INSTALL_DIR ###"

mkdir -p "$INSTALL_DIR"
install core/bgdc/src/bgdc "$INSTALL_DIR"
install core/bgdi/src/.libs/bgdi "$INSTALL_DIR"
install tools/moddesc/moddesc $INSTALL_DIR
find core -type f -name '*.so' -exec install '{}' "$INSTALL_DIR" ';'
find modules -type f -name '*.so' -exec install '{}' "$INSTALL_DIR" ';'

echo "### Build done! ###"

exit 0

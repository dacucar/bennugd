[BennuGD][bennugd] is a cross-platform 2D game programming language and 
engine.

I always found it tricky to build it in the linux distributions
I normally use so I decided to hold my own repository with the
modifications to make it easy for me to build and document the
required steps.

The official repository is hosted in [SourceForge.net][bennugd-sf].

[bennugd]: https://www.bennugd.org/
[bennugd-sf]: https://sourceforge.net/projects/bennugd/

To know what changed from original repo you can look at the commit
history (notice this git repository is exported from BennuGD SVN
repository, as I find it extremely handy to work with git rather
than SVN), but at the moment it is simply:

* The ``./build-linux.sh`` script
* The ``./configure`` scripts have ``+x`` permissions.
* I had to rename ``fmul``, ``fdiv`` functions in the 
  ``core/bgdrtm/src/fmath.c`` as newer versions of glibc already
  have those [names][glibc-changes] and a signature missmatch occurr. For 
  consistency I renamed also ``fcos`` and ``fsin``.

[glibc-changes]: https://sourceware.org/git/?p=glibc.git;a=commit;h=69a01461ee1417578d2ba20aac935828b50f1118

## Building on Fedora 29 x86_64

This is my currently workstation linux distribution.

```
sudo dnf groupinstall 'Development Tools'
sudo dnf install make autoconf automake glibc-devel.i686 SDL_mixer-devel.i686 \
	libX11-devel.i686 libpng-devel.i686 openssl-devel.i686 \
	SDL-devel.i686 zlib-devel zlib-devel.i686 libXrandr
```

Then you execute ``./build-linux.sh release`` and hope for the best :).
Do not forget to add the ``release`` argument the first time you build
as it will trigger the ``./configure`` which is needed to build.


## Building on other distributions

You will need gcc, autoconf, automake, glibc for 32bits and development
files for following libraries for i686 architecture.

* libX11
* libXrandr
* libpng
* openssl
* zlib
* SDL
* SDL_mixer

Each distribution uses different package names... use your package
manager to look for them.

I successfully managed to install it in OpenSUSE Tumbelweed, so you
should be able too.


## Run time dependencies dependencies

To be able to play ``.wav`` and ``.ogg`` install:
```
sudo dnf install SDL_sound.i686
```

To be able to play ``.midi`` files:
```
sudo dnf install libtimidity.i686
```

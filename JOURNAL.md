## 2023/03/26

The `rpath` doesn't work well with relative paths because the linker will resolve the 
path of the library relative to the current directory.

To check the current RPATH of an application we can do:

```
readelf -d ~/Apps/BennuGD/bgdi
...
 0x0000001d (RUNPATH)                    Library runpath: [/home/dacucar/Apps/BennuGD/lib:/usr/local/lib]
...
```

Notice that here it says RUNPATH and not RPATH as many internet links suggest.

It is technically possible to use the special string `${ORIGIN}`, in order to make it work I had to use

```
LDFLAGS='-Wl,-rpath,'\''$${ORIGIN}/lib'\'',--enable-new-dtags' \
```

## 2021/10/16

### Segmentation faults

I built BennuGD from sources and attempted to execute several BennuGD applications
but I'd get a `segmentation fault` error. The problem only happened for the applications
that use `mod_video`, so for example, running `./test vector` would work just fine.

Debugging with `gdb` showed that the issue is related to `SDL_SetVideoMode` returns 0.
BennuGD but will later attempt to dereference a structure member the (`PixelFormat`)
of the surface theoretically returned by `SDL_SetVideoMode`, which was 0.

The solution to this was... to not use the SDL 1.2 library, but use the SDL-Compat...
Not a great solution perhaps, but it's better than nothing.

This required:

* Cloning the git project of sdl-compat.
* Configuring cmake for 32bits compilation.
  ```
  +set(CMAKE_C_FLAGS -m32)
  +set(CMAKE_CXX_FLAGS -m32)
  ```
* Install dependencies to build SDL-Compat in 32 bits
  ```
  # These were added to build SDL-Compat
  sudo dnf install SDL2-devel.i686
  sudo dnf install mesa-libGL-devel.i686
  ```

### Failing to compile sources using mod_draw

This was a problem that I remembered. For some reason the `_inline` 
definitions in `libdraw.c` make the application not be able to find
certain functions. Removing the inline declarations made it work but
I'm not sure about the actual implications.

However, I don't understand why this is happening.


### Dependencies needed to build with autotools

I was missing some dependencies when attempting to run `autoreconf -vif`:

```bash
sudo dnf install SDL-devel.i686
sudo dnf install SDL_mixer-devel.i686
sudo dnf install make autoconf automake glibc-devel.i686 SDL_mixer-devel.i686 	libX11-devel.i686 libpng-devel.i686 openssl-devel.i686 	SDL-devel.i686 zlib-devel zlib-devel.i686 libXrandr
sudo dnf install libtool # Installs libtoolize

autoreconf -vif  
./configure --build=i686-pc-linux-gnu
make && sudo make install
```

The binaries are generated in the corresponding folders and the `make install`
installs in `/usr/local/bin` and `/usr/local/lib`.

For some reason, `/usr/local/lib` is not part of the default `ld` library 
path, so I added an entry to `/etc/ld.so.conf.d/`.

It should be possible to make `bgdc` and `bgdi` point to a relative lib path
by doing `LDFLAGS="-Wl,-rpath=../lib,--enable-new-dtags`.

### pkg-config not resolving SDL and SDL_mixer

	./configure --build=... complaining about SDL and SDL-mixer.

I can use sdl-config to get the path of the former, not sure how to
get the path from the other.

Perhaps a reboot will refresh pkg-config?

No, it didn't work.

I can't find the reason why, but I found the `*.pc` files for `sdl`
and `SDL_mixer` in `/usr/lib/pkgconfig` so I could use:

	export PKG_CONFIG_PATH=/usr/lib/pkgconfig

This make it work.

### Merged clean-up into master

I've replaced master branch with the clean-up one, as it starts to work.
